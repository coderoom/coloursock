<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WhatWeDo extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/whatWeDo
	 *	- or -
	 * 		http://example.com/index.php/whatWeDo/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/whatWeDo/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

		$this->load->helper('url');
    }
	
	public function index()
	{
		//$this->load->view('whatWeDo/whatWeDo');
		$data['main_content_url']	= 'whatwedo/whatwedo';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['home_resources']	= array
			(
				array("assets/img/crew/crewMembers/Jana.png","Video"),
				array("assets/img/crew/crewMembers/Jana.png","Photography"),
				array("assets/img/crew/crewMembers/Jana.png","Creative")
			);
		$data['last_resources']	= array
			(
				("assets/img/crew/crewMembers/Chuck.jpg"),
				("assets/img/crew/crewMembers/Chuck.jpg"),
				("assets/img/crew/crewMembers/Chuck.jpg"),
				("assets/img/crew/crewMembers/Chuck.jpg"),
				("assets/img/crew/crewMembers/Chuck.jpg"),
				("assets/img/crew/crewMembers/Chuck.jpg")
			);
		$data['side_bar_text']	= 'what we do';
		$data['main_topic']		= 'we take on mountains & make waves';
		$data['left_bar_description'] = 'capturing and stimulating the world- challenging the conventional and parting seas. We design and tell stories with every ounce of passion, intelligence and creativity we have, we make even most vanilla beginning find a colourful end.';
		
		$data['return_url']		= '';
		
		$this->load->view('templates/default.php', $data);
	}
	
	public function creative()
	{
		$data['main_content_url']	= 'templates/detailedView';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['resources']	= array
			(
				array("assets/img/whatWeDo/creative/img1.png","brand identity"),
				array("assets/img/whatWeDo/creative/img1.png","strategy"),
				array("assets/img/whatWeDo/creative/img1.png","copywrite"),
				array("assets/img/whatWeDo/creative/img1.png","print/digital execution")
			);
		$data['side_bar_text']	= 'creative';
		$data['main_topic']		= 'creative';
		$data['return_url']		= 'index.php/whatWeDo/';
		
		$this->load->view('templates/default.php', $data);
	}
	
	public function photography()
	{
		$data['main_content_url']	= 'templates/detailedView';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['resources']	= array
			(
				array("assets/img/whatWeDo/photography/img1.png","portrature"),
				array("assets/img/whatWeDo/photography/img1.png","product"),
				array("assets/img/whatWeDo/photography/img1.png","corporate"),
				array("assets/img/whatWeDo/photography/img1.png","editorial")
			);
		$data['side_bar_text']	= 'photography';
		$data['main_topic']		= 'photography';
		$data['return_url']		= 'index.php/whatWeDo/';
		
		$this->load->view('templates/default.php', $data);
	}
	
	public function video()
	{
		$data['main_content_url']	= 'templates/detailedView';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['resources']	= array
			(
				array("assets/img/whatWeDo/video/img1.png","stop motion"),
				array("assets/img/whatWeDo/video/img1.png","animation"),
				array("assets/img/whatWeDo/video/img1.png","vfx"),
				array("assets/img/whatWeDo/video/img1.png","etc")
			);
		$data['side_bar_text']	= 'video';
		$data['main_topic']		= 'video';
		$data['return_url']		= 'index.php/whatWeDo/';
		
		$this->load->view('templates/default.php', $data);
	}
}
