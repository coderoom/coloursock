<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Touch extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/touch
	 *	- or -
	 * 		http://example.com/index.php/touch/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/touch/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

		$this->load->helper('url');
    }
	
	public function index()
	{
		$this->load->view('touch/getInTouch');
	}
	
	public function joinOurArmy()
	{
		$this->load->view('touch/joinOurArmy');
	}
	
	public function subscribe()
	{
		$this->load->view('touch/subscribe');
	}
}
