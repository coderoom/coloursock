<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/projects
	 *	- or -
	 * 		http://example.com/index.php/projects/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/projects/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

		$this->load->helper('url');
    }
	
	public function index()
	{
		$data['main_content_url']	= 'templates/summaryView';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['home_resources']	= array
			(
				array("assets/img/projects/port1.png","GN ReSound"),
				array("assets/img/projects/port2.png","Coloursock"),
				array("assets/img/projects/project1.png","First Property"),
				array("assets/img/projects/port4.png","Blue Wealth"),
				array("assets/img/projects/port1.png","GN ReSound"),
				array("assets/img/projects/port2.png","Coloursock"),
				array("assets/img/projects/port3.png","Photography"),
				array("assets/img/projects/port4.png","Blue Wealth")
			);
		$data['side_bar_text']	= 'the projects';
		$data['main_topic']		= 'the projects';
		$data['left_bar_description'] = 'producing world rocking creative content that captures the heart and soul of your brand. 
							</br></br>we are movers and shakers stopping at nothing to help you create a splash of colour in this world.';
		
		$data['return_url']		= '';
		
		$this->load->view('templates/default.php', $data);
	}
	
	public function details()
	{
		$data['main_content_url']	= 'templates/detailedView';
		$data['leftbar_url']		= 'templates/crewDetailedLeftbar';
		$description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi vulputate ante eget ante elementum, a ornare ante vehicula. Nulla volutpat sed tellus congue molestie. Integer molestie non mi sit amet consequat. Duis blandit feugiat eros, vel cursus nulla tristique in. Nullam ac ante odio. Pellentesque vulputate efficitur tristique. Proin ac turpis eget urna rhoncus mollis in vel arcu. Vivamus consequat in felis consectetur elementum.';
		$data['resources']	= array
			(
				array("assets/img/projects/port1.png","GN ReSound", "content director - strategy and creative", $description),
				array("assets/img/projects/port2.png","Coloursock", "content director - strategy and creative", $description),
				array("assets/img/projects/project1.png","First Property", "content director - strategy and creative", $description),
				array("assets/img/projects/port4.png","Blue Wealth", "content director - strategy and creative", $description),
				array("assets/img/projects/port1.png","GN ReSound", "content director - strategy and creative", $description),
				array("assets/img/projects/port2.png","Coloursock", "content director - strategy and creative", $description),
				array("assets/img/projects/port3.png","Photography", "content director - strategy and creative", $description),
				array("assets/img/projects/port4.png","Blue Wealth", "content director - strategy and creative", $description),
			);
		$data['side_bar_text']	= 'project details';
		$data['main_topic']		= 'project details';
		$data['return_url']		= 'index.php/projects/';
		$data['dynamic_data']	= true;
		
		$this->load->view('templates/default.php', $data);
	}
	
	
}
