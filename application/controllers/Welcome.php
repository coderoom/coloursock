<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

		$this->load->helper('url');
    }
	
	public function index()
	{
		$data['carousel'] = array(
			"assets/img/home/Slider1.jpg",
			"assets/img/home/Slider2.png",
			"assets/img/home/Slider3.png",
			"assets/img/home/Slider4.jpg",
			"assets/img/home/Slider5.png",
		);
		
		$data['home_resources']	= array
			(
				array("assets/img/projects/port1.png","GN ReSound"),
				array("assets/img/projects/port2.png","Coloursock"),
				array("assets/img/projects/project1.png","First Property"),
				array("assets/img/projects/port4.png","Blue Wealth")
			);
		$this->load->view('welcome/index', $data);
	}
	
	public function about()
	{
		$this->load->view('welcome/about');
	}
	
}
