<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crew extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/crew
	 *	- or -
	 * 		http://example.com/index.php/crew/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/crew/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

		$this->load->helper('url');
    }
	
	public function index()
	{	
		$data['main_content_url']	= 'templates/summaryView';
		$data['leftbar_url']		= 'templates/detailedViewLeftbar';
		$data['home_resources']	= array
			(
				array("assets/img/crew/crewMembers/Chuck.jpg","Chuck"),
				array("assets/img/crew/crewMembers/Zoi.jpg","Zoi"),
				array("assets/img/crew/crewMembers/Luis.jpg","Luis"),
				array("assets/img/crew/crewMembers/Steph.jpg","Steph"),
				array("assets/img/crew/crewMembers/Rosanna.png","Rosanna"),
				array("assets/img/crew/crewMembers/Ben.jpg","Ben"),
				array("assets/img/crew/crewMembers/Benji.jpg","Benji"),
				array("assets/img/crew/crewMembers/Ali.jpg","Ali")
			);
		$data['side_bar_text']	= 'the crew';
		$data['main_topic']		= 'the crew';
		$data['left_bar_description'] = 'we know anything is impossible. 
									we are effortlessly equal in the depths of our diversity. 
									we breathe creativity. we get results, but ego is benine. 
									we see toys where others see computers. we are fueled by passion. 
									we are inspired by curiousity. we are bold and daring, but never blind.';
		
		$data['return_url']		= '';
		
		$this->load->view('templates/default.php', $data);
		
	}
	
	public function details()
	{
		$data['main_content_url']	= 'templates/detailedView';
		$data['leftbar_url']		= 'templates/crewDetailedLeftbar';
		$description = 'Steff, our cheeky (af) Chilean, German import, has an inextinguishable creative curiousity. She grew up on a farm in the chilly Chilean Patagonia, with her American/European parents (both vets, or as she would say ‘green peace parents’). Cause and affect of a multicultural upbringing has this one bestowed with an unrelinquishing itch for adventure and cultural discovery. At 19 the adventures began, she went looking for new landscapes, finding her way to South Africa’s Cape Town where she studied design and photography and fulfilled her dream of getting a decent tan. In pursuit of further professional growth, more sun tans and adventure, she found her way to Sydney and began a career in the world of advertising. Working simultaneously through a Bachelor in Communication (Social and Political Science), her next pursuit of adventure led this bundle of audacity to our doorstep. Steff, award winning Art Director, had us at ‘hello’. Her portfolio, not short of remarkable, detailed a career working with brands such as, Pernod Ricard (Absolut Vodka, G.H. Mumm and Chivas Regal), Coca Cola, Telstra, Chanel, ASOS, Audi and Disney just to name a few. Her pursuit to create social change is at the heart of everything she does. This half creative, half humanitarian is the perfect fit to Coloursock in her ambitious, world-rocking, selfless and flavourful approach to life.';
		$data['resources']	= array
			(
				array("assets/img/crew/crewMembers/Chuck.jpg","Chuck", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Zoi.jpg","Zoi", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Luis.jpg","Luis", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Steph.jpg","Steph", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Rosanna.png","Rosanna", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Ben.jpg","Ben", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Benji.jpg","Benji", "content director - strategy and creative", $description),
				array("assets/img/crew/crewMembers/Ali.jpg","Ali", "content director - strategy and creative", $description)
			);
		$data['side_bar_text']	= 'crew details';
		$data['main_topic']		= 'crew details';
		$data['return_url']		= 'index.php/crew/';
		$data['dynamic_data']	= true;
		
		$this->load->view('templates/default.php', $data);
	}
	
}
