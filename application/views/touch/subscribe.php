<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
		<meta name="viewport" content="width=device-width, initial-scale=1"> 
		<title>ColourSock</title>
		<meta name="description" content="ColourSock" />
		<meta name="keywords" content="ClourSock" />
		<meta name="author" content="Codrops" />

		<link href='https://fonts.googleapis.com/css?family=Roboto|Source+Sans+Pro|Crimson' rel='stylesheet'>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
    
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/listenBox.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/subscribe.css">	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/listenBox.css">

	</head>
	<body>
        <section id="section0">
            <a href="#" class="listen-box">
                <div class="listen-box-img">
                    <img src="<?php echo base_url(); ?>assets/img/home/listen.jpg">
                </div>
                <div class="listen-box-link">
                    <p>LISTEN</p>
                </div>
            </a>
            <div class="row" style="margin:0">
                <div class="col-lg-4 col-md-4 col-sm-6 left-col"> 
                <?php
					$this->load->view('templates/burger_menu');
				?>
                    <div class="description-box-wrapper">
                        <div class="descriptions-box">
                            <div class="description-header"><p>you b'in cheeky,</p></div>
                            <div class="wrapper-middle">
                                <div class="description-bar"><p>|</p></div>
                                <p>hehe, so are we </br><p style="color:#4162ff;">subscribe?</p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-6 right-col" style="padding:0">
                    <img class="bg" src="<?php echo base_url(); ?>assets/img/subscribe/bg.png">
                    <div class="form-wrapper">
                        <h1>we'll send you cool shit</h1>
                        <form>
                            <div class="form-icons">
                                <div class="input-group">
									<div class="input-group-content">
										<span class="input-group-label">
											<i class="fa fa-user"></i>
										</span>
										<input class="text-line" type="text" placeholder="NAME">
									</div>
                                </div>
                                <div class="input-group">
									<div class="input-group-content">
										<span class="input-group-label">
											<i class="fa fa-envelope"></i>
										</span>
										<input class="text-line" type="text" placeholder="EMAIL">
									</div>
                                </div>
								<div class="input-group">
							<div class="subscribe-btn-wrapper"><a class="subscribe-btn" id="btn2">subscribe</a></div>
								</div>
							</div>
                        </form>
                        
                    </div>
                    <div class="side-title">
                        <div class="side-title-content">
                            <div class="member-cross-wrapper"><a class="member-cross" href="<?php echo base_url();?>">&#10060;</a></div>
                            <div class="side-text"><h2>subscribe</h2></div>
                        </div>
                    </div>
                </div>
            </div>           
        </section>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<!-- /scripts -->
        <script src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
	</body>
</html>