<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=500" />
		<meta name="description" content="ColourSock" />
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/crew.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fullPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/whatWeDoCreative.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/scrollButton.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/sideNavBar.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/listenBox.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/main.css">
		
		<style>
.image-container {
  position: relative;
  width: 100%;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #99999970;
}

.image-container:hover .overlay {
  opacity: 1;
}

.text-main {
  color: white;
  font-size: 32px;
  font-family:crimson;
  letter-spacing: 3px;
  position: absolute;
  top: 43%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.text-sub {
  color: white;
  font-size: 12px;
  letter-spacing: 2px;
  position: absolute;
  top: 62%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.image-wrapper-col{
	margin: 5% 0% 0% 0%;
}
</style>
	</head>
	<body>
		<div class="row">
			<div class="col-xs-12 col-sm-4 fixed">
				<?php
					$this->load->view('templates/burger_menu');
				?>
				<div class="description-box-wrapper">
					<div class="descriptions-box">
						<p></p>
					</div>
				</div>
				<div class="do-not-click-box">
					<a href="<?php echo base_url(); ?>index.php/touch/subscribe"><img src="<?php echo base_url(); ?>assets/img/home/bin.jpg">DO NOT PRESS THIS BUTTON</a>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 nopadding scrollit">
				<!-- <div id="fullpage"> -->
					<div class="section" id="section0">
						<div class="row">
							<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
								<!-- Indicators -->
								<ol class="carousel-indicators">
								<?php
									$count = count($carousel);
									for($i=0; $i<$count; $i++){
										if($i==0){
								?>
											<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>" class="active"></li>
								<?php
										}else{
								?>
											<li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
								<?php
										}
									}
								?>
								</ol>
								
								<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
								<?php
									for($i=0; $i<$count; $i++){
										if($i==0){
								?>
											<div class="item active container">
								<?php
										}else{
								?>
											<div class="item container">
								<?php
										}
								?>
												<div class="row">
													<div class="side-nav">
														<ul class="side-nav-ul">
															<li><a href="<?php echo base_url(); ?>index.php/whatWeDo/Video">video</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/whatWeDo/Photography">photo</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/whatWeDo/Creative">creative</a></li>
														</ul>
													</div>
													<a href="#" class="listen-box">
														<div class="listen-box-img">
															<img src="<?php echo base_url(); ?>assets/img/home/listen.png">
														</div>
														<div class="listen-box-link">
															<p>LISTEN</p>
														</div>
													</a>
													<img class="bg-image" src="<?php echo base_url(); echo $carousel[$i];?>">
													<div id="scroll-down-link">
														<a class="scroll-down-a" href="#section1"><span></span>SCROLL DOWN</a>
													</div>
													<div class="carousel-caption">
														<img src="<?php echo base_url(); ?>assets/img/home/logo.png" class="img-responsive">
													</div>
												</div>
											</div>
									
								<?php
									}
								?>
								</div>
							</div>
						</div>
					</div>
					<div class="section" id="section1">
						<div class="row" id="pin">
							<div class="col-md-11">
								<div class="row">
									<div class="image-wrapper">
									<?php
									for($j=0; $j<4; $j++){
									?>
										<div class="col-sm-6 col-xs-12">
											<div class="image-wrapper-col">	
												<a href="details#<?php echo $i*4 + $j;?>">
													<div class="image-container">
														<img src="<?php echo base_url(); echo $home_resources[$j][0];?>" class="image"/>
														<div class="overlay">
															<p class="text-main"><?php echo $home_resources[$j][1];?></p>
															<p class="text-sub">VIEW DETAILS</p>
														</div>
													</div>
												</a>
											</div>
										</div>
									<?php
									}
									?>
									</div>
									<div class="legend-wrapper">
										<p class="title" style="color:white">.</p>
									</div>
								</div>
								<div class="portfolio-wrapper">
									<div class="show-more">
										<a href="<?php echo base_url(); ?>index.php/projects/">
											<img src="<?php echo base_url(); ?>assets/img/home/addMore.jpg">
											<div class="caption">MORE PROJECTS</div>
										</a>
									</div>
									<div class="space-occupier">
									</div>
								</div>
							</div>
							<div class="col-md-1">
								<div class="side-title">
									<h2>feature projects</h2>
								</div>
							</div>
						</div>
						<div class="row">
								<!-- Footer -->
								<div id="footer">
									<div class="row">
										<div class="col-md-4">
											<div class="footer-col">
												<p class="header">send us presents</p>
												<p class="subText">suite 211/53-59</p>
												<p class="subText">great buckingham street</p>
												<p class="subText">redfern, nsw 2016</p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="footer-col">
												<p class="header">call us on our cell phone</p>
												<p class="subText">+61 405 684 682</p>
												<br>
												<p class="header">spam us</p>
												<p class="subText"><a href="https://www.facebook.com/coloursock/">hello@coloursock.com</a></p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="footer-col">
												<a href="#section0"><span><img  class="footer-logo" src="<?php echo base_url(); ?>assets/img/home/smallLogo.png"></span></a>
											</div>
										</div>
									</div>
									<div class="row bottom-footer">
										<div class="col-md-3 social-links">
											<a href="https://www.facebook.com/coloursock/"><span><img  class="social-logo" src="<?php echo base_url(); ?>assets/img/home/fb.png"></span></a>
											<a href="https://vimeo.com/coloursock"><span><img  class="social-logo" src="<?php echo base_url(); ?>assets/img/home/v.png"></span></a>
											<a href="https://www.instagram.com/wearecoloursock/"><span><img  class="social-logo" src="<?php echo base_url(); ?>assets/img/home/sc.png"></span></a>
											<a href="https://www.behance.net/"><span><img  class="social-logo" src="<?php echo base_url(); ?>assets/img/home/be.png"></span></a>
										</div>										
										<div class="col-md-9 nav-bar-menu">
												<nav class="navbar navbar-default">
													<div class="container-fluid">
														<ul class="nav navbar-nav">
															<li><a href="<?php echo base_url(); ?>">home</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/about">our story</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/projects">projects</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/crew">the crew</a></li>
															<li><a href="<?php echo base_url(); ?>index.php/touch/joinOurArmy">join our army</a></li>
														</ul>
													</div>
												</nav>
										</div>
									</div>
								</div>
								<!-- footer END -->
						</div>
					</div>
				<!-- </div> -->
			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/scrolloverflow.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.fullPage.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/main.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/ScrollMagic.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/TweenMax.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/ScrollToPlugin.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/scrollingEffects.js"></script>
	</body>
</html>