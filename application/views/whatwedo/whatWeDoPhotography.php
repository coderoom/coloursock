<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="fullpage">
	<div class="section" id="section0">
		<div class="row">
			<div class="col-md-12" align="center">
				<div class="image-wrapper">
					<img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/whatWeDo/photography/img1.png">
				</div>
				<div class="legend-wrapper">
					<p class="title">portrature</p>
				</div>
				<div class="scroll-btn-wrapper">
					<div id="scroll-down-link" class="scroll-down">
						<a class="scroll-down-a" href="#1"><span></span>SCROLL DOWN</a>
					</div>
				</div>
			</div>					
		</div>
	</div>
	<div class="section" id="section1">
		<div class="row">
			<div class="col-md-12" align="center">
				<div class="image-wrapper">
					<img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/whatWeDo/photography/img2.png">
				</div>
				<div class="legend-wrapper">
					<p class="title">product</p>
				</div>
				<div class="scroll-btn-wrapper">
					<div id="scroll-down-link" class="scroll-down">
						<a class="scroll-down-a" href="#2"><span></span>SCROLL DOWN</a>
					</div>
				</div>
			</div>					
		</div>
	</div>
	<div class="section" id="section2">
		<div class="row">
			<div class="col-md-12" align="center">
				<div class="image-wrapper">
					<img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/whatWeDo/photography/img3.png">
				</div>
				<div class="legend-wrapper">
					<p class="title">corporate</p>
				</div>
				<div class="scroll-btn-wrapper">
					<div id="scroll-down-link" class="scroll-down">
						<a class="scroll-down-a" href="#3"><span></span>SCROLL DOWN</a>
					</div>
				</div>
			</div>					
		</div>
	</div>
	<div class="section" id="section3">
		<div class="row">
			<div class="col-md-12" align="center">
				<div class="image-wrapper">
					<img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/whatWeDo/photography/img4.png">
				</div>
				<div class="legend-wrapper">
					<p class="title">editorial</p>
				</div>
				<div class="scroll-btn-wrapper">
					<div id="scroll-top-link" class="scroll-top">
						<a class="scroll-top-a" href="#0"><span></span>BACK TO TOP</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>