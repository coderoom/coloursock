<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id="fullpage">
	<?php
		$id = 0;
		$count = count($resources);
		foreach ($resources as $resource) {
	?>
			<div class="section" id="section<?php echo $id;?>">
				<div class="row">
					<div class="col-md-12" align="center">
						<div class="image-wrapper">
							<img class="img-horizontal" src="<?php echo base_url(); echo $resource[0];?>">
						</div>
						<div class="legend-wrapper">
							<p class="title"><?php echo $resource[1];?></p>
						</div>
	<?php
					if($count > 1){
	?>
						<div class="scroll-btn-wrapper">
	<?php 
						if($id == $count - 1){
	?>
							<div id="scroll-top-link" class="scroll-top">
								<a class="scroll-top-a" href="#0"><span></span>BACK TO TOP</a>
							</div>
	<?php
						}
						else{
	?>
							<div id="scroll-down-link" class="scroll-down">
								<a class="scroll-down-a" href="#<?php echo $id+1;?>"><span></span>SCROLL DOWN</a>
							</div>
	<?php
						}
	?>
						</div>
	<?php
					}
	?>
						
					</div>					
				</div>
			</div>
	<?php
			$id = $id + 1;
		}
	?>

</div>