<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/what-we-do.css">
<div id="fullpage">
	<div class="section" id="whatwedo0">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="image-wrapper">
					<?php
					foreach($home_resources as $home_resource){
					?>
						<div class="col-sm-4">
							<div class="image-wrapper-col-right">
								<a href="<?php echo base_url(); ?>index.php/whatWeDo/<?php echo $home_resource[1]; ?>">
									<div class="image-container">
										<img src="<?php echo base_url(); echo $home_resource[0];?>" class="image">
										<div class="overlay">
											<div class="text"><?php echo $home_resource[1]; ?></div>
										</div>
									</div>
								</a>
							</div>
						</div>
					<?php
					}
					?>
					</div>
					<div class="legend-wrapper">
						<br/>
					</div>
				</div>
				<div class="row">
					<div class="scroll-btn-wrapper">
						<div id="scroll-down-link" class="scroll-down">
							<a class="scroll-down-a" href="#1"><span></span>SCROLL DOWN</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section" id="whatwedo1">
		<div class="row">
			<div class="col-md-12" style="padding-left:30px;padding-right:30px;">
					<div class="image-wrapper">
						<div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[0];?>">
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[1];?>">
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[2];?>">
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" style="height:100%">
										<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[3];?>">
										<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[4];?>">
								</div>
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<img class="placeholder-image" src="<?php echo base_url(); echo $last_resources[5];?>">
								</div>
							</div>
						</div>
					</div>
				<div class="legend-wrapper">
					<br/>
				</div>
				<div class="row">
					<div class="scroll-btn-wrapper">
						<div id="scroll-top-link" class="scroll-top">
							<a class="scroll-top-a" href="#0"><span></span>BACK TO TOP</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>