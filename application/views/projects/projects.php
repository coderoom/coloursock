<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/projects.css">
<style>
.image-container {
  position: relative;
  width: 100%;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  opacity: 0;
  transition: .5s ease;
  background-color: #99999970;
}

.image-container:hover .overlay {
  opacity: 1;
}

.text-main {
  color: white;
  font-size: 32px;
  font-family:crimson;
  letter-spacing: 3px;
  position: absolute;
  top: 43%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.text-sub {
  color: white;
  font-size: 12px;
  letter-spacing: 2px;
  position: absolute;
  top: 62%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.image-wrapper-col{
	margin: 5% 0% 0% 0%;
}
</style>
<div id="fullpage">
	<div class="section" id="projects0">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="image-wrapper">
						<?php
							foreach($home_resources as $home_resource){
						?>
							<div class="col-sm-6 col-xs-12">
								<div class="image-wrapper-col">	
									<a href="<?php echo base_url(); ?>index.php/projects/details<?php ?>">
										<div class="image-container">
											<img src="<?php echo base_url(); echo $home_resource[0];?>" class="image"/>
											<div class="overlay">
												<p class="text-main"><?php echo $home_resource[1];?></p>
												<p class="text-sub">VIEW PROJECT</p>
											</div>
										</div>
									</a>
								</div>
							</div>
						<?php
							}
						?>
					</div>
					<div class="legend-wrapper">
						<p class="title" style="color:white">.</p>
					</div>
				</div>
				<div class="row">
					<div class="scroll-btn-wrapper">
						<div id="scroll-down-link" class="scroll-down">
							<a class="scroll-down-a" href="#1"><span></span>SCROLL DOWN</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section" id="projects1">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="image-wrapper">
						<?php
							foreach($last_resources as $last_resource){
						?>
							<div class="col-sm-6 col-xs-12">
								<div class="image-wrapper-col">	
									<a href="<?php echo base_url(); ?>index.php/projects/details<?php ?>">
										<div class="image-container">
											<img src="<?php echo base_url(); echo $last_resource[0];?>" class="image"/>
											<div class="overlay">
												<p class="text-main"><?php echo $last_resource[1];?></p>
												<p class="text-sub">VIEW PROJECT</p>
											</div>
										</div>
									</a>
								</div>
							</div>
						<?php
							}
						?>
					</div>
					<div class="legend-wrapper">
						<p class="title" style="color:white">.</p>
					</div>
				</div>
				<div class="row">
					<div class="scroll-btn-wrapper">
						<div id="scroll-top-link" class="scroll-top">
							<a class="scroll-top-a" href="#0"><span></span>BACK TO TOP</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>