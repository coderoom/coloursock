<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=500" />
		<meta name="description" content="ColourSock" />
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/whatWeDoCreative.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fullPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/scrollButton.css">
	</html>
	<body>
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<div id = "burger-menu-wrapper">
					<header class="burger-header">
						<a href="index.html">
							<span><img src="<?php echo base_url(); ?>assets/img/home/smallLogo.png"></span>
						</a>
						<button class="hamburger">&#9776;</button>
						<button class="cross">&#10060;</button>
					</header>
							
					<div class="menu">
						<ul class="menu-ul">
							<li><a href="index.html">home</a></li>
							<li><a href="about.html">our story</a></li>
							<li><a href="whatWeDo.html">what we do</a></li>
							<li><a href="projects.html">projects</a></li>
							<li><a href="crew.html">the crew</a></li>
							<li><a href="getInTouch.html">get in touch</a></li>
							<br><br><br><br><br>
							<li><span><img src="<?php echo base_url(); ?>assets/img/home/add.jpg"></span><a href="joinOurArmy.html">join our army</a></li>
						</ul>
						<div class="client-login">
							<span><img src="<?php echo base_url(); ?>assets/img/home/clientLogin.jpg"><a href="#">CLIENT LOGIN</a></span>
						</div>
						<div class="clr"></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 pull-right nopadding">
				<?php
					$this->load->view($main_content);
				?>
			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/scrolloverflow.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.fullPage.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/whatWeDoCreative.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
	</body>
</html>