<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="side-title">
	<a class="member-cross" href="<?php echo base_url().$return_url; ?>">&#735;</a>
	<h2><?php echo $side_bar_text; ?></h2>
</div>