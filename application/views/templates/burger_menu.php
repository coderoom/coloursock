<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div id = "burger-menu-wrapper">
	<header class="burger-header">
		<a href="<?php echo base_url(); ?>">
			<span><img src="<?php echo base_url(); ?>assets/img/home/smallLogo.png"></span>
		</a>
		<button class="hamburger">&#9776;</button>
		<button class="cross">&#10060;</button>
	</header>
			
	<div class="menu">
		<ul class="menu-ul">
			<li><a href="<?php echo base_url(); ?>">home</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/welcome/about/">our story</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/whatWeDo/">what we do</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/projects/">projects</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/crew/">the crew</a></li>
			<li><a href="<?php echo base_url(); ?>index.php/touch/">get in touch</a></li>
			<br><br><br><br><br>
			<li><span><img src="<?php echo base_url(); ?>assets/img/home/add.jpg"></span><a href="<?php echo base_url(); ?>index.php/touch/joinOurArmy">join our army</a></li>
		</ul>
		<div class="client-login">
			<span><img src="<?php echo base_url(); ?>assets/img/home/clientLogin.jpg"><a href="#">CLIENT LOGIN</a></span>
		</div>
		<div class="clr"></div>
	</div>
</div>