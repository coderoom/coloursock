<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=500" />
		<meta name="description" content="ColourSock" />
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/whatWeDoCreative.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fullPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/scrollButton.css"/>
	</html>
	<body>
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<?php
					$this->load->view('templates/burger_menu');
					$this->load->view($leftbar_url);
				?>
			</div>
			<div class="col-xs-12 col-sm-1 pull-right">
				<?php
					$this->load->view('templates/side_title');
				?>
			</div>
			<div class="col-xs-12 col-sm-7 nopadding">
				<?php
					$this->load->view($main_content_url);
				?>
			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/scrolloverflow.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.fullPage.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/whatWeDoCreative.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
	</body>
</html>