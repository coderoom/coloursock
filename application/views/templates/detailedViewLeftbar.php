<div class="description-box-wrapper">
	<div class="descriptions-box">
		<div class="description-header"><p><?php echo $main_topic; ?></p></div>
		<div class="wrapper-middle">
			<div class="description-bar"><p>|</p></div>
			<?php
			if(isset($dynamic_data)){
				echo '<p class="name">Loading...</p>';
			} else if(isset($left_bar_description)){
				echo '<p>'.$left_bar_description.'</p>';
			}
			else{
				foreach ($resources as $resource) {
					echo '<p>'.$resource[1].'</p>';
				}
			}
			?>
		</div>
	</div>
</div>