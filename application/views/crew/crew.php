<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=500" />
		<meta name="description" content="ColourSock" />
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/crew.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.fullPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/scrollButton.css">
	</html>
	<body>
		<div class="row">
			<div class="col-xs-12 col-sm-4">
				<?php
					$this->load->view('templates/burger_menu');
				?>
				<div class="description-box-wrapper">
					<div class="descriptions-box">
						<div class="description-header"><p>the crew</p></div>
						<div class="wrapper-middle">
							<div class="description-bar"><p>|</p></div>
							<p>we know anything is impossible. 
									we are effortlessly equal in the depths of our diversity. 
									we breathe creativity. we get results, but ego is benine. 
									we see toys where others see computers. we are fueled by passion. 
									we are inspired by curiousity. we are bold and daring, but never blind.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-1 pull-right">
				<?php
					$this->load->view('templates/side_title');
				?>
			</div>
			<div class="col-xs-12 col-sm-7 nopadding">
					<div id="fullpage">
						<div class="section" id="section0">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="image-wrapper">
											<div class="image-wrapper-col-left col-sm-6 col-xs-12">
													<div class="image-wrapper-col-half-top">	
														<a href="crewDetails.html#member1"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Chuck.jpg"></a>
													</div>
													<div class="image-wrapper-col-half-bottom">
														<a href="crewDetails.html"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Zoi.jpg"></a>
													</div>
											</div>
											<div class="image-wrapper-col-right col-sm-6 col-xs-12">
													<div class="image-wrapper-col-half-top">
														<a href="crewDetails.html#member2"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Luis.jpg"></a>															
													</div>
													<div class="image-wrapper-col-half-bottom">
														<a href="crewDetails.html#member3"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Steph.jpg"></a>
													</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="scroll-btn-wrapper">
											<div id="scroll-down-link" class="scroll-down">
												<a class="scroll-down-a" href="#crew1"><span></span>SCROLL DOWN</a>
											</div>
										</div>
									</div>
								</div>
								
							</div>
						</div>
						<div class="section" id="section1">
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="image-wrapper">
											<div class="image-wrapper-col-left col-sm-6 col-xs-12">
													<div class="image-wrapper-col-half-top">												
														<a href="crewDetails.html"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Ali.jpg"></a>
													</div>
													<div class="image-wrapper-col-half-bottom">												
														<a href="crewDetails.html"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Ben.jpg"></a>
													</div>
											</div>
											<div class="image-wrapper-col-right col-sm-6 col-xs-12">
													<div class="image-wrapper-col-half-top">			
														<a href="crewDetails.html"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Benji.jpg"></a>
													</div>
													<div class="image-wrapper-col-half-bottom">
														<a href="crewDetails.html"><img src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Rosanna.png"></a>
													</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="scroll-btn-wrapper">
											<div id="scroll-top-link" class="scroll-top">
												<a class="scroll-top-a" href="#crew0"><span></span>BACK TO TOP</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		<div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/scrolloverflow.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.fullPage.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/crew.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
	</body>
</html>