<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8"> 
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <meta name="description" content="ColourSock" />
		<link href="<?php echo base_url(); ?>assets/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/crewDetails.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/burgerMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/scrollButton.css">
	</html>
	<body>
        <div id="header" style="height:10vh;">
        <div class="col-xs-12 col-sm-4">
            <?php
				$this->load->view('templates/burger_menu');
			?>
        </div>
        <div class="col-xs-12 col-sm-8 pull-right" > 
        </div>
        </div>
        <div id="fullpage">
            <section class="section " id="section0">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right">  
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Chuck.jpg">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div class="scroll-down" id="scroll-down-link">
                                            <a class="scroll-down-a" href="#member2"><span></span>SCROLL DOWN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section " id="section1">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right"> 
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Luis.jpg">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div class="scroll-down" id="scroll-down-link">
                                            <a class="scroll-down-a" href="#member3"><span></span>SCROLL DOWN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </section>
            <section class="section " id="section2">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right"> 
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Steph.jpg">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div class="scroll-down" id="scroll-down-link">
                                            <a class="scroll-down-a" href="#member4"><span></span>SCROLL DOWN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </section>
            <section class="section " id="section3">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right"> 
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-vertical" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Jana.png">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div class="scroll-down" id="scroll-down-link">
                                            <a class="scroll-down-a" href="#member5"><span></span>SCROLL DOWN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </section>
            <section class="section " id="section4">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right"> 
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-horizontal" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Lu.png">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div class="scroll-down" id="scroll-down-link">
                                            <a class="scroll-down-a" href="#member6"><span></span>SCROLL DOWN</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </section>
            <section class="section " id="section5">
                <div class="row memberSection">
                    <div class="col-xs-12 col-sm-4">
                        <div class="description-crew-wrapper">
                            <div class="descriptions-crew fadeElement">
                                <div class="detail-wrapper">
                                    <div class="name"></div>&nbsp<div class="designation"></div>
                                    </br></br>
                                    <div class="details">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 pull-right"> 
                        <div class="row">  
                            <div class="col-xs-11 col-sm-11">         
                                <div class="content-wrapper">
                                    <div class="space-wrapper">
                                    </div>
                                    <div class="image-wrapper">
                                        <img class="img-vertical" src="<?php echo base_url(); ?>assets/img/crew/crewMembers/Image1.png">
                                    </div>
                                    <div class="scroll-btn-wrapper">
                                        <div id="scroll-top-link">
                                            <a class="scroll-top-a" href="#member1"><span></span>BACK TO TOP</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-1 col-sm-1">
                                <div class="side-title fadeElement">
                                    <a class="member-cross" href="crew.html">&#735;</a>
                                    <h2></h2>
                                </div>
                            </div>
                        </div>                 
                    </div>
                </div>
            </section>
		<div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/scrolloverflow.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/vendor/jquery.fullPage.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/crewDetails.js"></script>
		<script src="<?php echo base_url(); ?>assets/js/burgerMenu.js"></script>
	</body>
</html>