$(document).ready(function() {
	var arr = [];
	var count = $('.section').length;
	for (var i = 0; i < count; i++) {
	   arr.push(''+i+'');
	}
    $('#fullpage').fullpage({
		
        anchors: arr,
        menu: '#menu',
		scrollingSpeed: 1200,
        responsiveWidth: 900,
        afterResponsive: function(isResponsive){
            
        },
		afterLoad: function(anchorLink, index){

			$(".name").html($(".name-"+index).html());
			$(".designation").html($(".hidden-title-"+index).html());
			$(".details").html($(".hidden-text-"+index).html());
			
			$(".name").fadeIn("slow");
			$(".designation").fadeIn("slow");
			$(".details").fadeIn("slow");
		},
		onLeave: function(index, nextIndex, direction){
			$(".name").fadeOut("slow");
			$(".designation").fadeOut("slow");
			$(".details").fadeOut("slow");
		}
    });
});
